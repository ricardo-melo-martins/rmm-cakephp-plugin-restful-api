<?php
declare(strict_types=1);

namespace ApiRestful\Controller\Devops;

use ApiRestful\Controller\AppController;
use Cake\Core\Configure;

class StatusController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
    }


    public function index()
    {

        $this->request->allowMethod(['get']);
        
        $data = [
            'api' =>  Configure::read('API_NAME' ),
            'version' => Configure::read('API_VERSION'),
            'timestamp' => time(),
        ];

        $this->set($data);
    }

}
