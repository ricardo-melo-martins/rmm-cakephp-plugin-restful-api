<?php
declare(strict_types=1);

namespace ApiRestful\Controller;

use ApiRestful\Controller\AbstractController as BaseController;
use Cake\Event\Event;
use Cake\Event\EventInterface;


class AppController extends BaseController
{
    
    public function initialize(): void
    {
        parent::initialize();
        
    }


    public function beforeRender(EventInterface $event)
    {
        parent::beforeRender($event);

    }

}
