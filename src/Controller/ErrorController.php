<?php

namespace ApiRestful\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventInterface;


class ErrorController extends AppController
{

    public function beforeRender(EventInterface $event)
    {
        parent::beforeRender($event);

        $this->viewBuilder()->setOption('serialize', true);
        $this->viewBuilder()->setClassName('ApiRestful.Error');
    }
}
