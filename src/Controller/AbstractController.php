<?php
declare(strict_types=1);

namespace ApiRestful\Controller;

use Cake\Controller\Controller as BaseController;
use Cake\Event\Event;
use Cake\Event\EventInterface;


class AbstractController extends BaseController
{

    private $_modelNameClass = null;


    private $_controllerNameClass = null;

    
    public function initialize(): void
    {
        parent::initialize();
        
        if(isset($this->modelClass) && !empty($this->modelClass))
        {
            $this->_modelClass = $this->modelClass;
        }

        $this->_controllerNameClass = $this->getName();
    }


    public function beforeRender(EventInterface $event)
    {
        parent::beforeRender($event);

        $this->viewBuilder()->setOption('serialize', true);
        $this->viewBuilder()->setClassName('ApiRestful.Api');
    }


    public function response($data = [])
    {
        
        if(is_array($data) && isset($data['error'])) {
            return $this->responseError($data['error']);
        }

        if(isset($data) && $data instanceof \Cake\ORM\Query 
            || isset($data) && $data instanceof \Cake\ORM\ResultSet 
            || isset($data) && $data instanceof \Cake\ORM\Entity){
            $data = $data->toArray();
            $this->set(compact('data'));
            return;
        } 

        $this->set(compact('data'));
    }


    public function responseError(string $message, array $validations = [], int $errorCode = null, string $errorSuggestion = null)
    {
        
        $error = [
            'message' => $message,
            'validations' => $validations,
            'code' => $errorCode,
            'suggestion' => $errorSuggestion
        ];

        $this->set(compact('error'));
    }
    
}
