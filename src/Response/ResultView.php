<?php
declare(strict_types=1);

namespace ApiRestful\Response;

use Cake\Core\Configure;

use Cake\Http\Response;
use Cake\Http\ServerRequest;

use Cake\ORM\Query;
use Cake\ORM\ResultSet;
use Cake\ORM\Entity;

use Exception;

class ResultView
{


    public function evaluate(ServerRequest $request, Response $response, $viewVars) {
        
        
        $statusCode = $response->getStatusCode();
        
        $contentDebug = null;
        
        $content = ['status' => $statusCode];

        if(!isset($viewVars)){
            throw new Exception("Error viewVars not defined", 1);
        }

        if(Configure::read('debug')){

            if(isset($viewVars['data']['debug'])){
                $contentDebug = $viewVars['data']['debug'];
                unset($viewVars['data']['debug']);
            }

            if(isset($viewVars['debug'])){
                $contentDebug = $viewVars['debug'];
                unset($viewVars['debug']);
            }
            
        }

        // Cakephp exception error range
        if($statusCode >= 400 && $statusCode < 600 ) {

            $content['error'] = (new ResultError)->message($response->getReasonPhrase(), [], $response->getStatusCode());
            
            $contentDebug = $viewVars;

            if(Configure::read('debug')){
                $content['debug'] = $contentDebug;
            }
            
            return json_encode($content);
        }

        if(is_array($viewVars) && isset($viewVars['error'])){
            
            $content['error'] = $viewVars['error'];

            if(Configure::read('debug')){
                $content['debug'] = $contentDebug;
            }

            return json_encode($content);
        }



        $dataArray = [];
        
        if(empty($viewVars)){

            $content['result']['data'] = $dataArray;
            
        } else {
            
            if($viewVars instanceof Query){
                $dataArray['data'] = $viewVars->toArray();

            } elseif ($viewVars instanceof ResultSet){
                $dataArray['data'] = $viewVars->toArray();

            } elseif ($viewVars instanceof Entity){
                $dataArray['data'] = $viewVars->toArray();

            } else {
                $dataArray = $viewVars;
            }
            
            $content['result'] = $dataArray;

            // paging
            if(isset($request->getAttributes()['paging'])) {
                if(is_array($request->getAttributes()['paging']) && isset(\array_values( $request->getAttributes()['paging'])[0])){
                    $content['result']['paging'] = \array_values( $request->getAttributes()['paging'])[0];
                }
            }

            // error - relative data
            if(is_array($viewVars) && isset($viewVars['error'])){
                $content['result']['error'] = $viewVars['error'];
            }
        }
    

        if(Configure::read('debug')){
            $content['debug'] = $contentDebug;
        }
        
        return json_encode($content);
    }
}
