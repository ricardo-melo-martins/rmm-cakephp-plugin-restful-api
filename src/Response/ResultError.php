<?php

declare(strict_types=1);

namespace ApiRestful\Response;

use Exception;

class ResultError
{

    public function message(string $message, array $validations = [], int $errorCode = null, string $errorSuggestion = null)
    {
        
        $error = [
            'message' => $message,
            'validations' => $validations,
            'code' => $errorCode,
            'suggestion' => $errorSuggestion
        ];

        return $error;
    }
    
}
