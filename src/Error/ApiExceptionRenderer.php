<?php

namespace ApiRestful\Error;

use ApiRestful\Controller\ErrorController;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Error\ExceptionRenderer;
use Cake\Error\Debugger;
use Cake\Core\Exception\Exception as CakeException;
use PDOException;
use Throwable;
use Psr\Http\Message\ResponseInterface;
use Cake\Collection\Collection;
class ApiExceptionRenderer extends ExceptionRenderer
{

    protected function _getController(): Controller
    {
        return new ErrorController();
    }

    public function missingController($exception)
    {
        return $this->evaluateResponse($exception, ['customMessage' => true]);
    }


    public function invalidTokenFormat($exception)
    {
        return $this->evaluateResponse($exception, ['customMessage' => true]);
    }


    public function invalidToken($exception)
    {
        return $this->evaluateResponse($exception, ['customMessage' => true]);
    }


    public function invalidArgument($exception)
    {
        return $this->evaluateResponse($exception, ['customMessage' => true]);
    }


    public function unexpectedValue($exception)
    {
        return $this->evaluateResponse($exception, ['customMessage' => true]);
    }


    public function recordNotFound($exception)
    {
        return $this->evaluateResponse($exception, ['customMessage' => true]);
    }


    public function missingHelper($exception)
    {
        return $this->evaluateResponse($exception, ['customMessage' => true]);
    }

    
    public function missingToken($exception)
    {
        return $this->evaluateResponse($exception, ['customMessage' => true]);
    }
    

    public function invalidCsrfToken($exception)
    {
        return $this->evaluateResponse($exception, ['customMessage' => true]);
    }

    
    private function evaluateResponse($exception, $options = []): ResponseInterface
    {
        // :TODO: validar versões do CakePHP entre 4.0 e 4.2 para usar _code ou getHttpCode
        // $code = $this->_code($exception); // FIXED: versões menores que CakePHP 4.2
        $code = $this->getHttpCode($exception);
        $method = $this->_method($exception);
        $template = $this->_template($exception, $method, $code);
        
        $this->clearOutput();

        $message = $this->_message($exception, $code);
        
        $url = $this->controller->getRequest()->getRequestTarget();
        $response = $this->controller->getResponse();

        if ($exception instanceof CakeException) {
            foreach ((array)$exception->responseHeader() as $key => $value) {
                $response = $response->withHeader($key, $value);
            }
        }
        
        $response = $response->withStatus($code);
        
        $viewVars = [
            'message' => $message,
            'url' => h($url),
            'code' => $code,
        ];

        $serialize = ['message', 'url', 'code'];

        $isDebug = Configure::read('debug');
        if ($isDebug) {
            $trace = (array)Debugger::formatTrace($exception->getTrace(), [
                'format' => 'array',
                'args' => false,
            ]);
            $origin = [
                'file' => $exception->getFile() ?: 'null',
                'line' => $exception->getLine() ?: 'null',
            ];
            
            array_unshift($trace, $origin);
            $viewVars['trace'] = $trace;
            $viewVars += $origin;
            $serialize[] = 'file';
            $serialize[] = 'line';
        }

        if ($exception instanceof CakeException && $isDebug) {
            $viewVars += $exception->getAttributes();
        }

        try {
            
            $evaluatedMessage = $this->evaluateMessage($viewVars);

        } catch (\Throwable $th) {

            $evaluatedMessage = [];
        }    
        
        $this->controller->set($evaluatedMessage);
        $this->controller->setResponse($response);
        $this->controller->render($response);
        return $this->_shutdown();        
        
        $builder = $this->controller->viewBuilder();
        $builder
            ->setHelpers([], false)
            ->setLayoutPath('')
            ->setTemplatePath('ApiRestful.Error')
            ->setOption('serialize', $serialize);
        
        $view = $this->controller->createView('View');
        
        $response = $this->controller->getResponse()
        ->withType('json')
        ->withStringBody($view->render(json_encode($template), 'error'));

        $this->controller->setResponse($response);
        $this->controller->render($response);


        return $this->_shutdown();
    }

    
    // :TODO: migrar para transform de resposta
    private function evaluateMessage(array $items){
        
        $collection = new Collection($items);

        $viewVars = $collection->each(function ($value, $key) {
            return [
                $key => $value
            ];
        });

        return $viewVars->toArray();
    }

}
