<?php

declare(strict_types=1);

namespace ApiRestful\View;

use Cake\View\JsonView;
use Cake\Core\Configure;

use Cake\Http\Response;
use Cake\Http\ServerRequest;

use ApiRestful\Response\ResultView;

use Exception;

class ErrorView extends JsonView
{

    public $responseType = 'json';


    public $defaultConfig = [
        'serialize' => null,
        'jsonOptions' => null,
        'jsonp' => null,
    ];


    private $hasRendered;


    public function initialize(): void
    {
        parent::initialize();
    }


    public function render(?string $template = null, $layout = null): string
    {
        if ($this->hasRendered) {
            return '';
        }
        
        return (new ResultView)->evaluate($this->request, $this->response, $this->viewVars);
    }

}
