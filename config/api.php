<?php

return [
    'API_NAME' => 'ApiRestful - RMM Cakephp Plugin Restful API',
    'API_VERSION' => '1.0.0',
    'Api' => [
        'debug' => [
            'active' => true
        ]
    ]
];