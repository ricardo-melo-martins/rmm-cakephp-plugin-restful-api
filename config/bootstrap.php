<?php


use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;


try {
    Configure::load('ApiRestful.api', 'default', false);
} catch (\Throwable $th) {
    throw $th;
}

