<?php

use Cake\Routing\RouteBuilder;
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\Router;

$routes->setRouteClass(DashedRoute::class);

// Api Common

$routes->scope('/api', function (RouteBuilder $builder) {
    
    $builder->setExtensions(['json']);

    // Devops 

    $builder->prefix('Devops', function (RouteBuilder $routes) {
        
        // api/server/ack
        $routes->connect(
            '/ack', 
            ['controller' => 'Status', 'action' => 'index']
        )->setMethods(['GET']);
        
    });


    $builder->fallbacks();
});