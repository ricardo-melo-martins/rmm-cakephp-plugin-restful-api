# RMM CakePHP Plugin Restful API

Plugin para acelerar a transformação do CakePhp MVC para Api de serviços Rest

## Requisitos

- Composer > 1.0
- Projeto CakePhp 4.x
- Git Bash para comandos de terminal


## Instalação

### Nova instalação do CakePHP
<br>
Se for um projeto novo, crie usando o comando

```bash
composer create-project --prefer-dist cakephp/app:4.* nome-do-diretorio
```

Este comando deve gerar o projeto dentro de uma pasta `nome-do-diretorio`

Acesse o diretório criado `nome-do-diretorio` e vamos testar a instalação

```bash
bin/cake server -p 8888
```

Abra o navegador e acesse a url local

```bash
http://localhost:8888
```

Agora acesse a url no navegador e caso não funcionar verique os passos de instalação em https://book.cakephp.org/4/pt/quickstart.html

Se funcionou, `Ctrl+C` para sair do terminal e continuamos a instalação

### Instalação do Plugin em um projeto existente

Abra um terminal e acesse o diretório criado `nome-do-diretorio` e rode o comamdo usando o composer para adicionar o repositorio do Plugin

Para versões do Composer maiores que > 1.0

```bash
composer config repositories.cakephp-plugin-api-restful vcs https://gitlab.com/ricardo-melo-martins/rmm-cakephp-plugin-restful-api
```

Para versões do Composer anteriores < 1.0

```bash
composer config repositories.cakephp-plugin-api-restful '{"name": "ricardo-melo-martins/rmm-cakephp-plugin-restful-api","type": "vcs","url": "https://gitlab.com/ricardo-melo-martins/rmm-cakephp-plugin-restful-api"}'
```

assim com este comando terminado, deve ter gerado no seu arquivo `composer.json` o repositório:

```bash
...
"repositories": [                         
  {
    "name": "ricardo-melo-martins/rmm-cakephp-plugin-restful-api",
    "type": "vcs",
    "url": "https://gitlab.com/ricardo-melo-martins/rmm-cakephp-plugin-restful-api"
  }                    
]   
...
```

Em seguida, instale o pacote:

```bash
composer require ricardo-melo-martins/rmm-cakephp-plugin-restful-api:dev-master
```

Rode o comando do `bin/cake` para adicionar o plugin ao projeto

```bash
bin/cake plugin load ApiRestful
```

E pronto!

Suba a aplicação e veja se o endpoint `status` responde sua requisição

```bash
bin/cake server -p 8888
```

Com isso sua aplicação deve responder na url `localhost:8888` e então o endpoint deve ser alcançado

```bash
http://localhost:8888/api/devops/status/ack
```

<br>

### Estratégias para respostas Json Rest

<br>

Se o seu projeto CakePhp não for MVC, assim pretende ser apenas Api recomendo trocar o Error Exception principal para que todas as saídas sejam formatadas, inclusive dos erros do CakePHP

- encontre o arquivo `config/app_local.php` e se não existir tente `config/app.php` e abra-o em um editor.

- encontre no código `use Cake\Error\ExceptionRenderer;` e substitua por `use ApiRestful\Error\ApiExceptionRenderer;`

- no início do arquivo ajuste para:

```bash

remover
use Cake\Error\ExceptionRenderer;

adicionar
use ApiRestful\Error\ApiExceptionRenderer as ExceptionRenderer;

Manter
'Error' => [
    'errorLevel' => E_ALL,
    'exceptionRenderer' => ExceptionRenderer::class, 
    'skipLog' => [],
    'log' => true,
    'trace' => true,
],


```

- por último, encontre o arquivo AppController.php ajuste para:

```bash

remover
use Cake\Controller\Controller;

adicionar
use ApiRestful\Controller\AbstractController as Controller;

```

### E

Exemplo de sucesso na requisição.

<br>

![Exemplo de consulta](docs/exemplo-consulta-ack.png)

<br>

Exemplo de erro na requisição

<br>

![Exemplo de consulta](docs/url-nao-existe.png)

<br>

Exemplo de erro na requisição com debug / trace habilitado

<br>

![Exemplo de consulta](docs/error-trace.png)

<br>

# Changelog

Em Desenvolvimento

## [Unreleased]

- Response - ajustar para tratar erros por escopo de aplicação e servidor (ApiView e ErrorView)

## [1.0.0] - 2021-03-06

## [Released]

- Criado documentação inicial para instalação usando composer
- Ajustando composer para baixar projeto sem publicação no Packagist (Temporário)
- Criado ApiException Renderer para normalização das respostas
- Template ApiView com validações de saída e controle de Http Status
- Esqueleto do plugin a partir do CakePHP 4.

<br>

# Todo

- Response - criar classe para tratar logs
- Response - Permitir ajustes no template de respostas
- Validar versões do CakePHP entre 4.0 e 4.2 para usar _code ou getHttpCode no ExceptionRenderer
- Middleware - Cors, Requests Logs, Filtros Content-Type, Firewall, Sanitize e Limits
- Routes - documentar uso de routes no plugin ou a partir da aplicação
- Templates para o Bake CakePHP Gerar classes (Controller, Models e Entity) baseadas neste Plugin
- Permitir configurações de range de erros e mensagens pelo app.php
- Usar eventos do Composer para instalar e ajustar as configurações
- Adicionar opção dinâmica de Sqlite para configuração e logs de requests
- Criar testes unitários
- migrar para outro repositorio interpessoal
- Publicar no Packagist
